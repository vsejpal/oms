package com.workday;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.util.List;
import java.nio.file.Files;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class RangeQueryBulkTest {
    private RangeContainer rc;
    @Before
    public void setUp(){
        List<String> lines = null;

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            URL url = classloader.getResource("data.txt");
            lines = Files.readAllLines(Paths.get(url.toURI()));
        } catch(Exception ex) {};
        String[] arr = lines.toArray(new String[lines.size()]);
        long[] data = new long[arr.length];
        for (int i = 0; i < arr.length; i++) {
            data[i] = Long.parseLong(arr[i]);
        }

        RangeContainerFactory rf = new YourRangeContainerFactory();
        rc = rf.createContainer(data);
    }
    @Test
    // runs in 2074153 ns when using YourRangeContainer
    // runs in 1293210 ns when using YourRangeContainerAlternative
    public void runARangeQuery(){
        long startTime = System.nanoTime();
        Ids ids = rc.findIdsInRange(500, 600, true, true);
        assertEquals(1028, ids.nextId());
        assertEquals(2729, ids.nextId());
        assertEquals(4922, ids.nextId());
        assertEquals(5803, ids.nextId());
        assertEquals(7233, ids.nextId());
        assertEquals(8042, ids.nextId());
        assertEquals(8131, ids.nextId());
        assertEquals(9510, ids.nextId());
        assertEquals(Ids.END_OF_IDS, ids.nextId());
        assertEquals(Ids.END_OF_IDS, ids.nextId());
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        System.out.println("Execution time in nano-seconds : " +
                timeElapsed);
    }
}
