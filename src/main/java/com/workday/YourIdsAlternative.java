package com.workday;

import java.util.PriorityQueue;
/*
    Generator to be used with "YourRangeContainerAlternative"
 */
public class YourIdsAlternative implements Ids {

    private PriorityQueue<Short> pq;


    public YourIdsAlternative(PriorityQueue<Short> pq) {
        this.pq = pq;
    }

    @Override
    public short nextId() {
        if(pq.isEmpty()) {
            return Ids.END_OF_IDS;
        }

        return pq.poll();
    }



}
