package com.workday;

import java.util.*;
/*
    Alternative Container that uses extra space and should perform better if the ranges in the query are moderately narrow.
 */
public class YourRangeContainerAlternative implements RangeContainer {
    private Map<Long, List<Short>> map;
    public YourRangeContainerAlternative(long[] data) {

        this.map = new HashMap<>(); // map to store value -> [index list]

        for(short i = 0; i < data.length; i++) {
            long val = data[i];
            if (map.containsKey(val)) {
                map.get(val).add(i);
            }
            else {
                map.put(val, new ArrayList<Short>());
                map.get(val).add(i);
            }
        }
    }

    @Override
    public Ids findIdsInRange(long fromValue, long toValue, boolean fromInclusive, boolean toInclusive) {
        PriorityQueue<Short> pq = new PriorityQueue<Short>(); // min-heap of indexes so we can maintain the order
        if (fromInclusive) {
            for (short s : map.getOrDefault(fromValue, new ArrayList<Short>())) {
                pq.add(s);
            }
        }

        if (toInclusive) {
            for (short s : map.getOrDefault(toValue, new ArrayList<Short>())) {
                pq.add(s);
            }
        }

        for (long i = fromValue + 1; i < toValue; i++) {
            for (short s : map.getOrDefault(i, new ArrayList<Short>())) {
                pq.add(s);
            }
        }


        return new YourIdsAlternative(pq);
    }
}
