package com.workday;

public class YourRangeContainer implements RangeContainer {
    private long[] data;
    public YourRangeContainer(long[] data) {
        this.data = data;
    }

    @Override
    public Ids findIdsInRange(long fromValue, long toValue, boolean fromInclusive, boolean toInclusive) {
        return new YourIds(data, fromValue, toValue, fromInclusive, toInclusive);
    }


}
