package com.workday;

public class YourIds implements Ids {
    private long[] data;
    private long fromValue, toValue;
    private boolean fromInclusive, toInclusive;
    private short currentIndex;

    public YourIds(long[] data, long fromValue, long toValue, boolean fromInclusive, boolean toInclusive) {
        this.data = data;
        this.fromValue = fromValue;
        this.toValue = toValue;
        this.fromInclusive = fromInclusive;
        this.toInclusive = toInclusive;
        this.currentIndex = 0;
    }

    @Override
    public short nextId() {
        while (currentIndex < data.length) {
            if (data[currentIndex] > fromValue && data[currentIndex] < toValue) {
                return currentIndex++;
            }

            if (fromInclusive && data[currentIndex] == fromValue) {
                return currentIndex++;
            }

            if (toInclusive && data[currentIndex] == toValue) {
                return currentIndex++;
            }

            currentIndex++;
        }

        return Ids.END_OF_IDS;
    }
}
